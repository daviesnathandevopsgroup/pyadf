from pyadf.inline_nodes.marks.mark import Mark

class Emphasis(Mark):
    type = 'em'
    def __init__(self):
        super(Emphasis, self).__init__()