from pyadf.inline_nodes.marks.mark import Mark

class Code(Mark):
    type = 'code'
    def __init__(self):
        super(Code, self).__init__()