if [ $# -eq 0 ]
  then
    echo "You must supply the version number to this script."
fi

twine upload dist/pyadf-$1-py3-none-any.whl